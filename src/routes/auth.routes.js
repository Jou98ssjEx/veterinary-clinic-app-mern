import { Router } from 'express'
import { check } from 'express-validator';
import { googleSingIn, register, renewToken, singIn } from '../controllers/auth.controller.js';
import { isExistUserEmail } from '../helpers/db-content-validation.js';
import { isNotExistUser, isUserActive, matchPassword, validateContent } from '../middlewares/validate-content.js';
import { validateJwt } from '../middlewares/validate-jwt.js';

export const routerAuth = Router();

// Registrar usuario
routerAuth.post('/register',[
   check('name', 'Name is required').not().isEmpty(), 
   check('email', 'Email is required').not().isEmpty(),
   check('email', 'Email: example@example.com').isEmail(),
   check('email', 'Email already exists').custom(isExistUserEmail),
   check('password', 'Password is required').not().isEmpty(),
   check('password', 'Password is min 6 caracter').isLength({min: 6}),
   check('rol', 'Rol is required').not().isEmpty(),
   validateContent,
] ,  register);

// Login
routerAuth.post('/singin', [
    check('email', 'Email is required').not().isEmpty(),
    check('email', 'Email: example@example.com').isEmail(),
    check('password', 'Password is required').not().isEmpty(),
    check('password', 'Password is min 6 caracter').isLength({min: 6}),
    validateContent,
    isNotExistUser,
    isUserActive, // si el usuario esta activo
    matchPassword,
], singIn);

// Login con google
routerAuth.post('/google', googleSingIn);

// Renovar token
routerAuth.get('/renew', validateJwt, renewToken );