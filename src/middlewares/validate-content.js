import bcryptjs from 'bcryptjs';
import { request, response } from 'express'
import { validationResult } from 'express-validator';
import { UserModel } from '../models/user.model.js';

// todas las validaciones por check()
export const validateContent = ( req = request , res =  response, next ) => {
    
    const errors = validationResult(req);

    if( !errors.isEmpty()){
        return res.status(400).json({ok: false, errors});
    }

    next();
}

// checar email: esta registrado o no
export const checkEmail = async ( req = request , res =  response  ) => {

    const { email } = req.body;

    const userFound = await UserModel.findOne({email});

    // if(!userFound){
    //     return res.status(400).json({
    //         ok: false,
    //         msg: `Registred user con email: ${email}`,
    //     })
    // }


    return userFound;

}

// SI el usuario no esta registrado
export const isNotExistUser = async ( req = request , res =  response, next ) => {

    const { email } = req.body;

    // const userFound = await UserModel.findOne({email});

    const userFound = await checkEmail( req, res);
    if( !userFound ) {
        return res.status(401).json({
            ok: false,
            msg: `This email: ${email}, din´t registered`,
        })
    }

    next();

}

// Si el usuario esta registrado
export const isUserRegisted = async ( req = request , res =  response, next ) => {

    // const { email } = req.body;

    // const userFound = await UserModel.findOne({email});

    const userFound = await checkEmail(req, res);
    if( userFound ) {
        return res.status(401).json({
            ok: false,
            msg: `This email: ${email}, already registered`,
        })
    }

    next();

}

// Si el usuario esta activo
export const isUserActive = async ( req = request , res =  response, next ) => {

    // const { email } = req.body;

    // const userFound = await UserModel.findOne({email});

    const userFound = await checkEmail(req, res);
    if(!userFound.status){
        return res.status(401).json({
            ok: false,
            msg: `User has been disabled`
        })
    }
    next();

}

// SI la contraseña hace mactch
export const matchPassword = async ( req = request , res =  response, next ) => {

    const { password } = req.body;

    const userFound = await checkEmail(req, res);

    const validatePass = bcryptjs.compareSync(password, userFound.password);
    // console.log({validatePass})
    if (!validatePass) {
        return res.status(400).json({
            ok: false,
            msg: 'Email and Password incorrect - pass'
        });
    }

    next();


}