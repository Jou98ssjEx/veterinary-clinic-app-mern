import mongoose from "mongoose";
import 'colors';

export const dbConfig = async( ) => {
    const dbProd = 'Produccion';
    try {
       await mongoose.connect(process.env.MONGO_DB);
       console.log(`DataBase Online in: ${ dbProd.green} `)
    } catch (error) {
        console.log(error);
        throw new Error('Error connecting the database');
    }

}