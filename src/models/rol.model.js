import { Schema, model } from 'mongoose'

const RoleSchema = Schema({
    rol: {
        type: String
    }
});

module.exports = model('Role', RoleSchema);
