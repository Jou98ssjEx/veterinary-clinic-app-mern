// const { OAuth2Client } = require('google-auth-library');
import { OAuth2Client } from 'google-auth-library'

const CLIENT_ID = process.env.GOOGLE_CLIENT_ID;

const client = new OAuth2Client(CLIENT_ID);

async function verify(token = '') {
    // console.log(token)
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });

    const { picture, name, email } = ticket.getPayload();
    // const payload = ticket.getPayload();
    // console.log({payload})
    
    // console.log(picture, name, email)
    // const userid = payload['sub'];
    // If request specified a G Suite domain:
    // const domain = payload['hd'];

    return {
        img: picture,
        name,
        email
    }
}
// verify().catch(console.error); // funciona aun con la alerta

export default verify;
// module.exports = {
//     verify
// }
