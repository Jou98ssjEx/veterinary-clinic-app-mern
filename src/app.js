import { Server } from "./Server/server.js";
import dotenv from 'dotenv'

console.clear();
dotenv.config();

const server = new Server();
server.listenner();