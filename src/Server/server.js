import express from 'express';
import cors from 'cors'
import 'colors';
import { dbConfig } from '../database/dbConfig.js';
import { routerAuth } from '../routes/auth.routes.js';

const url = `/api`
export class Server {

    
    constructor( ) {
        this.app = express();
        this.port = process.env.PORT;
        this.urlPath ={
            auth: `${url}/auth`,
        }

        this.middlewares();
        this.dbConnect();
        this.routes();
    }

    async dbConnect(){
        await dbConfig();
    }

    middlewares(){

         // Cors
        // Permite las peticiones de cualquier URL
        this.app.use(cors());

        // Lectura y parseo de JSON
        // El manejo de la información en formato JSON
        this.app.use(express.json());

        // Carpeta publica estatica
        this.app.use(express.static('src/public'));

    }

    routes(){
        this.app.use(this.urlPath.auth, routerAuth);
    }

    listenner(){
        this.app.listen( this.port, () => {
            console.log(`Server Online in port: ${ this.port.green}`)
        })
    }
}

