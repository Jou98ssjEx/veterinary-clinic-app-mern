import { request, response } from 'express';
import bcryptjs from 'bcryptjs';
import { UserModel } from '../models/user.model.js';
import { generateToken } from '../helpers/jsonWebToken.js';
import verify from '../helpers/google-verify.js';
import { checkEmail } from '../middlewares/validate-content.js';

export const register = async ( req = request, res = response) => {

    const { email } = req.body;
    try {
        // Verificar si el usuario existe por email
        let user = await UserModel.findOne( { email } );

        // Verificar si el usuario existe 
        if(user){
            return res.status(400).json({
                ok: false, 
                // msg: `Usuario ya a sido registrado`,
                msg: `User already registered`,
            })
        }

        // Crear el usuario en la db
        user = new UserModel(req.body);

        // // verificar el status
        // if( !user.status ){
        //     return res.status(401).json({
        //         ok: false,
        //         // msg: `Usuario esta desabilitado`,
        //         msg: `User is disabled`,
        //     })
        // }

        // Encriptar la contraseña
        // Encrypt the password
        const salt = bcryptjs.genSaltSync(11);
        user.password = bcryptjs.hashSync(user.password, salt);
    
        // Guardar la data en la db
        // Save the data in the db
        await user.save(); 
    
         // generar JWT
        const token = await generateToken(user.id);
        //  const token = await generateToken(user.id);
        // console.log({
        //     user
        // })
        res.json({
            ok: true,
            // msg: `User Registrado`,
            msg: `User Register`,
            user,
            token
        })
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        })
    }
}

export const singIn = async ( req = request, res =  response ) => {

    // const { email, password } = req.body;

    try {

            const user = await checkEmail(req, res);
            // console.log({user});
            // console.log(user.id);
            // Choca con validacion si existe usuario

            // ruta: validar si esta activo o no

            // ruta: match Password

            
            // generar JWT
            const token = await generateToken(user.id);


            res.json({
                ok: true,
                msg: 'Login',
                user,
                // uid: user.id,
                // name: user.name,
                token
            })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        })
    }
}

export const googleSingIn = async ( req = request, res =  response ) => {

    const { tokenID } = req.body;
    try {

        const { email, name, img } = await verify(tokenID);

        let user = await UserModel.findOne({ email });
        // console.log(user);

        if (!user) {
            const data = {
                name,
                email,
                img,
                password: ':P',
                google: true,
                rol: 'User_Role',
                // rol: 'USER_ROLE'
            }

            user = new UserModel(data);
            await user.save();
        } 
        // else {
        //     return res.status(401).json({
        //         ok: false,
        //         msg: `User exist, use auth with google`
        //     })
        // }

        // Si el user tiene stado en false
        // If the user's status is set to false
        if (!user.status) {
            return res.status(401).json({
                msg: ' User esta desabilitado'
            })
        }

        // generar JWT

        const token = await generateToken(user.id);
        
       res.json({
            ok: true,
            msg: 'Login con google',
            uid: user.id,
            name,
            email,
            google: user.google,
            img,
            token
        }) 
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        })
    }
}

export const renewToken = async ( req = request, res =  response ) => {

    try {
        const { userAuth } = req;

        //     // Usuario autenticado : userAuth 
            const token = await generateToken(userAuth.id);
        
        res.json({
            ok: true,
            msg: 'Renovar token',
            uid: userAuth.id,
            name: userAuth.name,
            token,
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        })
    }
}



// const googleSingIn = async(req = request, res = response) => {

//     const { token_google } = req.body;
//     // console.log(token_google);
//     try {
//         const { email, name, img } = await verify(token_google);

//         let user = await UserModel.findOne({ email });
//         console.log(user);

//         if (!user) {
//             const data = {
//                 name,
//                 email,
//                 img,
//                 password: ':P',
//                 google: true,
//                 rol: 'USER_ROLE'
//             }

//             user = new UserModel(data);
//             await user.save();
//         }

//         // Si el user tiene stado en false
//         // If the user's status is set to false
//         if (!user.status) {
//             return res.status(401).json({
//                 msg: ' User esta desabilitado'
//             })
//         }

//         // generar JWT

//         const token = await generarToken(user.id);

//         res.json({
//             msg: ' Todo Ok',
//             user,
//             token
//             // token_google
//         });

//     } catch (error) {
//         console.log(error);
//         res.status(401).json({
//             msg: 'no se pudo verificar el token',
//             error
//         })
//     }


// }
// ==================================

//         const user = await UserModel.findOne({ email });

//         if( !user){
//             return res.status(400).json({
//                 ok: false,
//                 msg: 'Email din´t registered'
//             })
//         }
//         // console.log(user.status)
//         // Si el usuario esta activo
// //         // If the user is active
//         if (!user.status) {
//             return res.status(400).json({
//                 ok: false,
//                 // msg: 'Email and Password incorrect - status'
//                 msg: `User has been disabled`
//             });
//         }

// //         // Si la password hace macth
//         const validatePass = bcryptjs.compareSync(password, user.password);

//         if (!validatePass) {
//             return res.status(400).json({
//                 ok: false,
//                 msg: 'Email and Password incorrect - pass'
//             });
//         }
